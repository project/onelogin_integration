<?php

use Drupal\Core\Database\Database;
use Drupal\user\RoleInterface;

/**
 * Implements hook_install().
 */
function onelogin_integration_install() {
  // User won't be able to change his username, prevent possible problems.
  user_role_revoke_permissions(RoleInterface::AUTHENTICATED_ID, ['change own username']);
}

/**
 * Implements hook_schema().
 */
function onelogin_integration_schema() {
  $schema['onelogin_authmap'] = [
    'description' => 'Stores Onelogin authentication mapping.',
    'fields' => [
      'aid' => [
        'description' => 'Primary Key: Unique authmap ID.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'uid' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => "User's {users}.uid.",
      ],
    ],
    'primary key' => ['aid'],
    'indexes' => [
      'uid' => ['uid'],
    ],
    'foreign keys' => [
      'user' => [
        'table' => 'users',
        'columns' => ['uid' => 'uid'],
      ],
    ],
  ];

  return $schema;
}

/**
 * Implements hook_requirements().
 */
function onelogin_integration_requirements($phase) {
  $requirements = [];

  if ($phase != 'runtime') {
    return $requirements;
  }

  // Check for the PHP OpenSSL library.
  if (!function_exists('openssl_random_pseudo_bytes')) {
    $requirements['onelogin_integration_openssl'] = [
      'value' => t('Not installed'),
      'severity' => REQUIREMENT_ERROR,
      'description' => t('OneLogin SAML PHP Toolkit requires the use of the <a href="@openssl">Open SSL</a> to handling x509 certificates. see <a href="@php-saml">PHP SAML Toolkit</a> for dependency updates.', ['@openssl' => 'http://php.net/manual/en/book.openssl.php', '@php-saml' => 'https://github.com/onelogin/php-saml#dependences']),
    ];
  }
  else {
    $requirements['onelogin_integration_openssl'] = [
      'value' => t('Installed'),
      'severity' => REQUIREMENT_OK,
    ];
  }

  // Check for the Gettext library.
  if (!function_exists('gettext')) {
    $requirements['onelogin_integration_gettext'] = [
      'value' => t('Not all features enabled.'),
      'severity' => REQUIREMENT_WARNING,
      'description' => t('Install <a href="@gettext">Gettext Library</a> and its php driver. It handles translations.', ['@gettext' => 'http://php.net/manual/en/book.gettext.php']),
    ];
  }
  else {
    $requirements['onelogin_integration_gettext'] = [
      'value' => t('Installed'),
      'severity' => REQUIREMENT_OK,
    ];
  }

  $requirements['onelogin_integration_openssl']['title'] = t('OneLogin Integration, OpenSSL Library');
  $requirements['onelogin_integration_gettext']['title'] = t('OneLogin Integration, Gettext Library');

  return $requirements;
}

/**
 * Create onelogin_authmap table.
 */
function onelogin_integration_update_8001(&$sandbox) {
  $schema = Database::getConnection()->schema();
  if (!$schema->tableExists('onelogin_authmap')) {

    $table_schema = [
      'description' => 'Stores Onelogin authentication mapping.',
      'fields' => [
        'aid' => [
          'description' => 'Primary Key: Unique authmap ID.',
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ],
        'uid' => [
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
          'description' => "User's {users}.uid.",
        ],
      ],
      'primary key' => ['aid'],
      'indexes' => [
        'uid' => ['uid'],
      ],
      'foreign keys' => [
        'user' => [
          'table' => 'users',
          'columns' => ['uid' => 'uid'],
        ],
      ],
    ];

    $schema->createTable('onelogin_authmap', $table_schema);
  }
}
