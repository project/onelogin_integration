<?php

namespace Drupal\onelogin_integration;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Password\DefaultPasswordGenerator;

/**
 * Class UserService for the OneLogin Integration module.
 *
 * Creates a user with the username / password coming from the OneLogin account
 * the user uses to login with. This service is triggered when someone is trying
 * to log in through OneLogin, but no account exists yet for the given
 * username / email.
 *
 * @package Drupal\onelogin_integration
 */
class UserService implements UserServiceInterface {

  /**
   * The variable that holds an instance of ConfigFactoryInterface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * The variable that holds an instance of EntityTypeManagerInterface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The variable that holds an instance of the MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  private $messenger;

  /**
   * The variable that holds an instance of the DefaultPasswordGenerator.
   *
   * @var \Drupal\Core\Password\DefaultPasswordGenerator
   */
  private $passwordGenerator;

  /**
   * UserService constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Reference to ConfigFactoryInterface.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Reference to TypeManagerInterface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger_interface
   *   Reference to MessengerInterface.
   * @param \Drupal\Core\Password\DefaultPasswordGenerator $password_generator
   *   Reference to Password Generator.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    MessengerInterface $messenger_interface,
    DefaultPasswordGenerator $password_generator
  ) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger_interface;
    $this->passwordGenerator = $password_generator;
  }

  /**
   * Creates a user.
   *
   * @param string $username
   *   The username for the new user.
   * @param string $email
   *   The email for the new user.
   *
   * @return mixed
   *   Returns a user object.
   */
  public function createUser($username, $email) {
    $user = $this->entityTypeManager->getStorage('user')->create(
      [
        'name'                     => $username,
        'mail'                     => $email,
        'pass'                     => $this->passwordGenerator->generate(10),
        'enforceIsNew'             => TRUE,
        'init'                     => $email,
        'defaultLangcode'          => 'en',
        'preferred_langcode'       => 'en',
        'preferred_admin_langcode' => 'en',
        'status'                   => 0,
      ]
    );

    $user->save();
    $this->entityTypeManager->getStorage('user')->load($user->id());

    $this->messenger->addMessage("User with uid " . $user->id() . " saved!\n");

    return $user;
  }

}
